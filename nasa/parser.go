package nasa

import (
	"fmt"
	"io/ioutil"
	"net/http"

	"gitlab.com/mirekwalczak/url-collector/internal/provider"
)

// Parse fetches data from NASA API
func Parse(url string, client http.Client, parseData chan provider.ParseData) {
	resp, err := client.Get(url)
	if err != nil {
		parseData <- provider.New("", resp.StatusCode, fmt.Errorf("cannot reach url %q", url))
		return
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		parseData <- provider.New("", resp.StatusCode, fmt.Errorf("error: %q", err))
		return
	}

	if resp.StatusCode != http.StatusOK {
		parseData <- provider.New("", resp.StatusCode, fmt.Errorf("error in response from API"))
		return
	}
	nasaData, err := Marshal(body)
	if err != nil {
		parseData <- provider.New("", resp.StatusCode, fmt.Errorf("error: %q", err))
		return
	}

	parseData <- provider.New(nasaData.URL, 0, nil)
}
