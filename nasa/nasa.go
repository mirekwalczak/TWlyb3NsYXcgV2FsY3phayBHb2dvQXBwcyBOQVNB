package nasa

import (
	"encoding/json"
)

type Nasa struct {
	URL string `json:"url"`
}

type NasaURLs struct {
	URLs []Nasa `json:"urls"`
}

func Marshal(data []byte) (*Nasa, error) {
	var n *Nasa
	err := json.Unmarshal(data, &n)
	if err != nil {
		return nil, err
	}

	return n, nil
}

func PrepareURLs(ps []Nasa) NasaURLs {
	var urls NasaURLs
	urls.URLs = ps
	return urls
}
