package main

import (
	"log"
	"net/http"

	"gitlab.com/mirekwalczak/url-collector/config"
)

func main() {
	port := config.GetEnv("PORT", ":8080")
	log.Printf("service url-collector listening on port %s", port)
	http.HandleFunc("/pictures", getPictures)
	if err := http.ListenAndServe(port, nil); err != nil {
		log.Fatalf("error: listening and serving: %s", err)
	}
}
