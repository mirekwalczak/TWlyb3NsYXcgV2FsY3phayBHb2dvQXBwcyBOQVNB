package main

import (
	"fmt"
	"net/http"

	"gitlab.com/mirekwalczak/url-collector/config"
	"gitlab.com/mirekwalczak/url-collector/date"
	"gitlab.com/mirekwalczak/url-collector/internal/platform/web"
	"gitlab.com/mirekwalczak/url-collector/internal/provider"
	"gitlab.com/mirekwalczak/url-collector/nasa"
)

func getPictures(w http.ResponseWriter, r *http.Request) {
	// TODO Move business logic to separate package

	if r.Method == "GET" {
		from := r.URL.Query().Get("from")
		to := r.URL.Query().Get("to")
		err := date.ValidateRange(from, to)
		if err != nil {
			web.RespondError(w, http.StatusBadRequest, err)
			return
		}

		dates, err := date.GetDates(from, to)
		if err != nil {
			web.RespondError(w, http.StatusInternalServerError, fmt.Errorf("cannot calculate date range %q", err))
			return
		}
		apiKey := config.GetEnv("API_KEY", "DEMO") // move to main
		baseurl := fmt.Sprintf("https://api.nasa.gov/planetary/apod?api_key=%s_KEY", apiKey)
		var nasaData []nasa.Nasa // change package name or structure
		// concurrentRequests, err := strconv.Atoi(config.GetEnv("CONCURRENT_REQUESTS", "5"))
		if err != nil {
			web.RespondError(w, http.StatusInternalServerError, fmt.Errorf("error parsing a number"))
			return
		}

		task := make(chan provider.ParseData)
		var client http.Client // add some TTLs etc
		for _, date := range dates {
			url := fmt.Sprintf("%s&date=%s", baseurl, date)

			// TODO Allow to determine the amount of goroutines
			go nasa.Parse(url, client, task)
		}
		for range dates {
			pd := <-task
			if pd.Error != nil {
				web.RespondError(w, pd.StatusCode, pd.Error)
				return
			}

			nasaData = append(nasaData, nasa.Nasa{URL: pd.PhotoURL})
		}

		output := nasa.PrepareURLs(nasaData)

		web.Respond(w, output, http.StatusOK)
	}
}
