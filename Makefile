SHELL := /bin/bash

run:
	go run ./cmd/service

test:
	go test ./...

tidy:
	go mod tidy

docker:
	docker build -t url-collector .

compose:
	docker-compose up

