# media-downloader
Media downloader is a service that parses image urls from the NASA API.


## Getting started

To run service locally use
```
make run
```

To run service using Docker
```
make compose
```
