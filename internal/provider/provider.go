package provider

import (
	"net/http"
)

type Provider interface {
	Parse(url string, client http.Client, parseData chan ParseData)
}

type ParseData struct {
	PhotoURL   string
	StatusCode int
	Error      error
}

func New(photoURL string, statusCode int, err error) ParseData {
	return ParseData{photoURL, statusCode, err}
}
