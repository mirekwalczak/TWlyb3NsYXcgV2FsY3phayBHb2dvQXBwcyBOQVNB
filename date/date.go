package date

import (
	"fmt"
	"time"
)

// GetDates generates slice of dates in string format out of date range
func GetDates(from, to string) ([]string, error) {
	const template = "2006-01-02"
	var dates []string
	fromDate, err := time.Parse(template, from)
	if err != nil {
		return []string{}, fmt.Errorf("wrong start date")
	}
	toDate, err := time.Parse(template, to)
	if err != nil {
		return []string{}, fmt.Errorf("wrong end date")
	}
	for d := fromDate; !d.After(toDate); d = d.AddDate(0, 0, 1) {
		dates = append(dates, d.Format(template))
	}

	return dates, nil
}

// ValidateRange checks if the dates are correct
func ValidateRange(from, to string) error {
	if from == "" || to == "" {
		return fmt.Errorf("you must provide date range")
	}

	const template = "2006-01-02"

	fromDate, err := time.Parse(template, from)
	if err != nil {
		return fmt.Errorf("wrong start date")
	}
	toDate, err := time.Parse(template, to)
	if err != nil {
		return fmt.Errorf("wrong end date")
	}
	if fromDate.After(toDate) {
		return fmt.Errorf("end date cannot be befeore start date")
	}

	duration := toDate.Sub(fromDate)
	if duration.Hours()/24 > 10 {
		return fmt.Errorf("cannot get range higher than 10 days")
	}

	return nil
}
