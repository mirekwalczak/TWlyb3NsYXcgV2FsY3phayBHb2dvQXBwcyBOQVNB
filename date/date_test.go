package date

import (
	"reflect"
	"strings"
	"testing"
)

func TestGetDates(t *testing.T) {
	dates, err := GetDates("2019-12-28", "2020-01-02")
	if err != nil {
		t.Errorf("GetDates failed: %s", err)
	}

	want := []string{"2019-12-28", "2019-12-29", "2019-12-30", "2019-12-31", "2020-01-01", "2020-01-02"}
	if !reflect.DeepEqual(dates, want) {
		t.Errorf("Should be (%v) but is (%v)", dates, want)
	}
}

func TestValidateRange(t *testing.T) {
	var tests = []struct {
		from string
		to   string
		want string
	}{
		{"2019-12-28", "", "you must provide date range"},
		{"", "2020-01-28", "you must provide date range"},
		{"2019-12-28", "2019-12-26", "end date cannot be befeore start date"},
		{"abc", "2019-12-28", "wrong start date"},
		{"2019-12-28", "abc", "wrong end date"},
		{"2019-12-28", "2020-02-28", "cannot get range higher than 10 days"},
	}

	for _, test := range tests {
		err := ValidateRange(test.from, test.to)
		if !strings.Contains(err.Error(), test.want) {
			t.Errorf("ValidateRange(%s, %s) returned \"%s\" error message, want \"%s\"",
				test.from, test.to, err.Error(), test.want)
		}
	}
}
